import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'InfiniteScroll';

  cars = [];

  lazyCars;

  brands: string[];

  colors: string[];

  totalLazyCarsLength: number;

  timeout: any;

  sortKey: string;

  sortOptions;

  pageIndex = 10;

  constructor() { }

  ngOnInit() {
    this.brands = [
      'Audi', 'Fiat', 'Ford', 'Honda', 'Jaguar', 'Mercedes', 'Renault', 'Volvo', 'VW'
    ];

    this.colors = [
      'Black', 'White', 'Red', 'Blue', 'Silver', 'Green', 'Yellow'
    ];

    this.totalLazyCarsLength = 10000;

    for (let i = 0; i < this.totalLazyCarsLength; i++) {
      this.cars.push(this.generateCar());
    }

    //in a real application, make a remote request to retrieve the number of records only, not the actual records
    

    this.sortOptions = [
      { label: 'Newest First', value: '!year' },
      { label: 'Oldest First', value: 'year' }
    ];
    console.log(this.cars, this.cars.length);
  }

  generateCar() {
    return {
      vin: this.generateVin(),
      brand: this.generateBrand(),
      color: this.generateColor(),
      year: this.generateYear()
    }
  }

  generateVin() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  generateBrand() {
    return this.brands[Math.floor(Math.random() * Math.floor(10))];
  }

  generateColor() {
    return this.colors[Math.floor(Math.random() * Math.floor(7))];
  }

  generateYear() {
    return 2000 + Math.floor(Math.random() * Math.floor(19));
  }

  loadCarsLazy(event) {
    console.log(event);
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page

    //imitate db connection over a network
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.lazyCars = [];
      if (this.cars) {
        this.lazyCars = this.cars.slice(event.first, (event.first + event.rows));
      }
    }, 1000);
  }

  onSortChange() {
    if (this.sortKey.indexOf('!') === 0)
      this.sort(-1);
    else
      this.sort(1);
  }

  sort(order: number): void {
    let cars = [...this.cars];
    cars.sort((data1, data2) => {
      let value1 = data1.year;
      let value2 = data2.year;
      let result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (order * result);
    });

    this.cars = cars;
  }

  pageUpdate(event) {
    console.log(event);
    console.log(event.page + 1);
    this.pageIndex = 100;
  }


}
