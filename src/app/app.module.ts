import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PaginatorModule} from 'primeng/paginator';


import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    VirtualScrollerModule,
    BrowserAnimationsModule,
    PaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
  
 }
